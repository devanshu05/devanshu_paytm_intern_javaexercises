import java.io.File;
import java.util.Scanner;
public class DirectoryList{
	public static void main(String[] args){
		String directory_name;
		File directory;
		String[] files;
		Scanner sc;
		sc=new Scanner(System.in);
		System.out.print("Enter the directory path: ");
		directory_name=sc.nextLine().trim();
		directory=new File(directory_name);

		if(directory.isDirectory()==false){
			if(directory.exists()==false)
				System.out.println("directory doesn't exists");
			else
				System.out.println("Missing file");

		}
		else{
			files = directory.list();
			System.out.println("Files in directory \""+directory+"\":");
			for(int i=0;i<files.length;i++)
				{
					System.out.println(" " + files[i]);
				}
		}



	}
}