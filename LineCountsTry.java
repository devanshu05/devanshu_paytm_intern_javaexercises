import java.io.*;
import java.util.*;
public class LineCountsTry{
   public static void main(String[] args){
      if(args.length==0){
         System.out.println(" No files to read, pls mention filename");
      }
      for(int i=0;i<args.length;i++){
         System.out.print(args[i]+ ": ");
         countLines(args[i]);
      }
   }
   private static void countLines(String fileName) {
        String line = "";
        int lineCount = 0;
        try(BufferedReader bufferedReader = new BufferedReader(
                new FileReader(fileName))) {
            while ((line = bufferedReader.readLine()) != null) {
                lineCount++;  
            }
            System.out.println("Number of lines is : "
                    + lineCount);  
        }
        catch (FileNotFoundException fnfex) {
            fnfex.printStackTrace();
        }
        catch (IOException ioex) {
            ioex.printStackTrace();
        }
    }
}